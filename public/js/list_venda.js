$(document).ready(function($) {
    var table = $("#table").DataTable({
        ajax: "venda/list",
        scrollCollapse: true,
        responsive: true,
        paging: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        searching: true,
        "pageLength": 5,
        "order": [0, "ASC"],
        columns: [
            { data: "cliente", name: "cliente" },
            { data: "produto", name: "produto" },
            { data: "quantidade", name: "quantidade" },
            { data: "valor_final", name: "valor_final" },
        ],
        language: { url: "//cdn.datatables.net/plug-ins/1.10.25/i18n/Portuguese-Brasil.json" }
    });

});