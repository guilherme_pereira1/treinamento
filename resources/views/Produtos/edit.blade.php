@extends('layouts.app')

@section('htmlheader_title', 'Produto')
@section('contentheader_title', 'Produto')
@section('links_adicionais') 
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  @endsection
@section('conteudo')


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Alterar Produto: {{$produto->titulo}}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/home">Home</a></li>
                        <li class="breadcrumb-item active">Produto</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    (<span style="color: red;">*</span>) Campos Obrigatórios
                        <div class="float-right">
                            <a href="{{ URL::to('produto') }}" class="btn btn-block btn-outline-info "><i class="fa fa-list-alt"></i> Listar</a>
                        </div>
                    </div>
                    <div class="card-body">
                         @if (Session::has('messagem'))
                            <div class="alert {{ Session::get('class') }} alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5>Atenção</h5>
                                {{ Session::get('messagem') }}
                            </div>
                        @endif

                        <form method="POST" action="/produto/{{$produto->id}}" id="form">
                            @csrf
                            @method('PUT')
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <strong>Título<span style="color: red;">*</span></strong>
                                    <input type="text" autocomplete="off" name="titulo" class="form-control @error('titulo') is-invalid @enderror" 
                                    value="{{ $produto->titulo }}">
                                    @error('titulo')
                                         <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                               
                                <div class="form-group col-md-3">
                                    <strong>Descrição<span style="color: red;">*</span></strong>
                                    <input type="text" autocomplete="off" name="descricao" class="form-control @error('descicao') is-invalid @enderror" 
                                    value="{{ $produto->descricao }}">
                                    @error('descricao')
                                         <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <strong>Quantidade<span style="color: red;">*</span></strong>
                                    <input type="text" autocomplete="off" name="quantidade" class="form-control @error('quantidade') is-invalid @enderror" 
                                    value="{{ $produto->quantidade }}">
                                    @error('quantidade')
                                         <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <strong>Valor<span style="color: red;">*</span></strong>
                                    <input type="text" autocomplete="off" name="valor" class="form-control @error('valor') is-invalid @enderror" 
                                    value="{{ $produto->valor }}" >
                                    @error('valor')
                                         <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <hr>
                            <button type="submit" form="form" class="btn btn-info float-right" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>
                            &nbsp Aguarde...">Salvar</button>

                            <!-- /.card-body -->
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                    <!-- /.card -->
            </div>
        </div>
    </section>

@endsection
@section('scripts_adicionais') 

@endsection
