@extends('layouts.app')

@section('htmlheader_title', 'Venda')
@section('contentheader_title', 'Venda')
@section('links_adicionais')
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endsection
@section('conteudo')


<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Adicionar Venda</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item active">Venda</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    (<span style="color: red;">*</span>) Campos Obrigatórios
                    <div class="float-right">
                        <a href="{{ URL::to('venda') }}" class="btn btn-block btn-outline-info "><i class="fa fa-list-alt"></i> Listar</a>
                    </div>
                </div>
                <div class="card-body">
                    @if (Session::has('messagem'))
                    <div class="alert {{ Session::get('class') }} alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5>Atenção</h5>
                        {{ Session::get('messagem') }}
                    </div>
                    @endif


                    <form method="POST" action="/venda" id="form">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <strong>Cliente<span style="color: red;">*</span></strong>
                                <select id="cliente" name='cliente' class="select2 form-control  @error('cliente') is-invalid @enderror" required>
                                    <option value="">Selecione</option>
                                    @foreach($cliente as $cliente)
                                    <option value='{{$cliente->id}}'>{{$cliente->nome}}</option>
                                    @endforeach
                                </select>
                                @error('cliente')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <strong>Produto<span style="color: red;">*</span></strong>
                                <select id="produto" name='produto' class="select2 form-control  @error('produto') is-invalid @enderror" required>
                                    <option value="">Selecione</option>
                                    @foreach($produto as $produto)
                                    <option value='{{$produto->id}}'>{{$produto->titulo}}</option>
                                    @endforeach
                                </select>
                                @error('produto')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                            </div>
                            <div class="form-group col-md-6">
                                <strong>Quantidade<span style="color: red;">*</span></strong>
                                <input type="text" autocomplete="off" name="quantidade" class="form-control @error('quantidade') is-invalid @enderror" value="{{ old('quantidade') }}">
                                @error('quantidade')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                            <button type="submit" form="form" class="btn btn-info float-right" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>
                            &nbsp Aguarde...">Salvar</button>

                            <!-- /.card-body -->
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</section>

@endsection
@section('scripts_adicionais')

@endsection