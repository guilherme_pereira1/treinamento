@extends('layouts.app')

@section('htmlheader_title', 'Cliente')
@section('contentheader_title', 'Cliente')
@section('links_adicionais') 
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  @endsection
@section('conteudo')


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Adicionar Cliente</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/home">Home</a></li>
                        <li class="breadcrumb-item active">Cliente</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    (<span style="color: red;">*</span>) Campos Obrigatórios
                        <div class="float-right">
                            <a href="{{ URL::to('cliente') }}" class="btn btn-block btn-outline-info "><i class="fa fa-list-alt"></i> Listar</a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (Session::has('messagem'))
                            <div class="alert {{ Session::get('class') }} alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5>Atenção</h5>
                                {{ Session::get('messagem') }}
                            </div>
                        @endif

                     
                        <form method="POST" action="/cliente" id="form">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <strong>Nome<span style="color: red;">*</span></strong>
                                    <input type="text" autocomplete="off" name="nome1" class="form-control @error('nome1') is-invalid @enderror" 
                                    value="{{ old('nome1') }}" >
                                    @error('nome1')
                                         <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <strong>Telefone<span style="color: red;">*</span></strong>
                                    <input type="telefone" autocomplete="off" name="telefone" class="form-control @error('telefone') is-invalid @enderror" 
                                    value="{{ old('telefone') }}">
                                    @error('telefone')
                                         <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-3">
                                    <strong>Nascimento<span style="color: red;">*</span></strong>
                                    <input type="date" autocomplete="off" name="nascimento" class="form-control @error('nascimento') is-invalid @enderror" 
                                    value="{{ old('nascimento') }}">
                                    @error('nascimento')
                                         <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <strong>E-mail<span style="color: red;">*</span></strong>
                                    <input type="email" autocomplete="off" name="email" class="form-control @error('email') is-invalid @enderror" 
                                    value="{{ old('email') }}" >
                                    @error('email')
                                         <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <hr>
                            <button type="submit" form="form" class="btn btn-info float-right" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>
                            &nbsp Aguarde...">Salvar</button>

                            <!-- /.card-body -->
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                    <!-- /.card -->
            </div>
        </div>
    </section>

@endsection
@section('scripts_adicionais') 

@endsection
