<?php

namespace App\Http\Controllers;
use Session;
use DataTables;

use App\Http\Requests\RequestProduto;
use App\Produto;
use App\Cliente;
use Illuminate\Http\Request;

use Redirect;

class ProdutoController extends Controller
{
    public function index(){
        $produto = Produto::get();

        return view('Produtos.index',compact('produto'));
    }

    public function create(){
        $cliente = Cliente::select('id','nome')->get();
        return view('Produtos.create', compact('cliente'));

    }

    public function store(RequestProduto $request){
        try{    
            $produto = new Produto();
            $produto->titulo = $request->titulo;
            $produto->descricao = $request->descricao;
            $produto->valor = $request->valor;
            $produto->quantidade = $request->quantidade;

            $produto->save();

                Session::flash('messagem','Parabéns, produto adicionado com sucesso.');
                Session::flash('class','alert-success');
                return back()->withInput();           

        } catch (\Exception  $errors) {
            Session::flash('messagem','Ops ERRO!!, não foi possível add produto.');
            Session::flash('class','alert-danger');
            return back()->withInput();
        }
        
        
    }
    public function show($id)
    {
        //
    }

    public function list(){
        $produto = Produto::get();
        #dd($produto);
        return Datatables::of($produto)->editColumn('opcoes', function ($produto) {
            return '
                <a href="/produto/'.$produto->id.'/edit" class="btn btn-block btn-outline-secondary"><i class="fas fa-user-edit"></i> Editar produto</a>
                <button class="btn btn-block btn-outline-danger btnExcluir" data-id="'.$produto->id.'" data-nome="'.$produto->titulo.'" type="button"> <i class="fas fa-trash"></i>Excluir</button>'
            ;
        })->escapeColumns([0])->make(true);
    }

    public function edit($id){
        $produto = Produto::find($id);
        return view('Produtos.edit', compact('produto'));
    }

    public function update(RequestProduto $request, $id){
        try{    
            $produto = Produto::find($id);
            $produto->titulo = $request->titulo;
            $produto->descricao = $request->descricao;
            $produto->valor = $request->valor;
            $produto->quantidade = $request->quantidade;

            $produto->save();

                Session::flash('messagem','Parabéns, produto Alterado com sucesso.');
                Session::flash('class','alert-success');
                return back()->withInput();           

        } catch (\Exception  $errors) {
            Session::flash('messagem','Ops ERRO!!, não foi possível Alterar produto.');
            Session::flash('class','alert-danger');
            return back()->withInput();
        }
    }

    public function destroy($id)
    {
        try{    
            Produto::destroy($id);
            return response()->json(array('status' => "OK"));       

        } catch (\Exception  $errors) {
            return response()->json(array('erro' => "$errors"));
        }
        
    }
}
