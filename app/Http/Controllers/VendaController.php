<?php

namespace App\Http\Controllers;
use DataTables;
use App\Produto;
use App\Venda;
use App\Cliente;
use Session;
use App\Http\Requests\RequestVendas;

use Illuminate\Http\Request;

class VendaController extends Controller
{
    public function index()
    {
        $venda = Venda::get();

        return view('Venda.index', compact('venda'));
    }
    public function create()
    {
        $cliente = Cliente::select('id', 'nome')->get();
        $produto = Produto::select('id', 'titulo')->get();
        
        return view('Venda.create', compact('cliente', 'produto'));
    }
    public function list(){
        $venda = Venda::get();
        return Datatables::of($venda)->escapeColumns([0])->make(true);;
    }
    public function store(RequestVendas $request)
    {
        $produto = Produto::where('id', $request->produto)->select('valor')->first();
        $produto_quantidade = Produto::where('id', $request->produto)->select('quantidade');
            try {
                $venda = new Venda();
                $var = floatval($produto->valor);
                $aux2 = intval($request->quantidade);
                $venda->cliente = $request->cliente;
                $venda->produto = $request->produto;
                $venda->quantidade = $request->quantidade;
                $venda->valor_final = $var * $aux2;
                $venda->save();

                Session::flash('messagem', 'Parabéns, venda adicionado com sucesso.');
                Session::flash('class', 'alert-success');
                return back()->withInput();
            } catch (\Exception  $errors) {
                Session::flash('messagem', 'Ops ERRO!!, não foi possível add venda.');
                Session::flash('class', 'alert-danger');
                return back()->withInput();
            
        }

    }
}
