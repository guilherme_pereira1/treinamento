<?php

use Illuminate\Database\Seeder;

class ProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert([
            'titulo'  =>  'Pão de Queijo',
            'descricao'  =>  'Feito com queijo cuia',
            'valor'  =>  '15,00',
            'quantidade' =>  '10',
            'created_at' =>  now()
        ] );
    }
}
